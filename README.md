# CART API
[Live on Heroku](https://joyjet-fnscoder.herokuapp.com/api/auth/token/)

username: joyjet

password: joyjetpass

## How to run the project

1. Clone repository
2. Create a virtualenv python 3.7
3. Activate your virtualenv
4. Install dependencies (gunicorn and psycopg2 are not required for development instance, only for deploy on heroku)
5. Configure the instance with .env file
6. Create user
7. Run the tests

```console
git clone git@bitbucket.org:fnscoder/joyjet.git joyjet
cd joyjet
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
cp contrib/sample-env .env
python manage.py createsuperuser
python manage.py test
```

## Example URL's and json data

### Root
Get /api/

### JWT Token
POST /api/auth/token/

### Articles
POST /api/articles/

{"name": "water","price": 100}

GET /api/articles/

GET /api/articles/1/

### Carts
POST /api/carts/

{"items": []}

GET /api/carts/

GET /api/carts/1/

POST PUT /api/carts/1/add_to_cart/

{"items": [{"article_id": 1, "quantity": 2}, {"article_id": 2, "quantity": 3}]}

GET /api/carts/1/checkout/

### Discounts

POST /api/discount_types/

{"discount_type": "discount_type_name"}

GET /api/discount_types/

GET /api/discount_types/1/

POST /api/discounts/1/add_discount/

{"discount_type": 1, "value": 25}

GET /api/discounts/

GET /api/discounts/1/
