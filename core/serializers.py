from rest_framework import serializers

from .models import Article, Cart, CartItem, Discount, DiscountType


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'name', 'price')


class CartSerializer(serializers.ModelSerializer):
    items = serializers.StringRelatedField(many=True)

    class Meta:
        model = Cart
        fields = ('id', 'created_at', 'updated_at', 'checkout', 'total', 'delivery_date', 'items')


class CartItemSerializer(serializers.ModelSerializer):
    cart = CartSerializer(read_only=True)
    article = ArticleSerializer(read_only=True)

    class Meta:
        model = CartItem
        fields = ('id', 'cart', 'article', 'quantity')


class DiscountTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscountType
        fields = ('id', 'discount_type')


class DiscountSerializer(serializers.ModelSerializer):
    article = ArticleSerializer(read_only=True)
    discount_type = DiscountTypeSerializer()

    class Meta:
        model = Discount
        fields = ('id', 'article', 'discount_type', 'value')


class CheckoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = ('id', 'total')
