from datetime import datetime

from django.test import TestCase
from ..models import Article, Cart, CartItem


class ArticleModelTest(TestCase):
    def setUp(self):
        self.obj = Article(
            name='water',
            price='100'
        )
        self.obj.save()

    def test_create_article(self):
        """Article object must exist"""
        self.assertTrue(Article.objects.exists())

    def test_str_(self):
        """str method must return article's name"""
        self.assertEqual('water', str(self.obj))


class CartModelTest(TestCase):
    def setUp(self):
        self.obj = Cart()
        self.obj.save()

    def test_create_cart(self):
        """Cart object must exist"""
        self.assertTrue(Cart.objects.exists())

    def test_created_at(self):
        """Cart objects must have an auto created_at attr"""
        self.assertIsInstance(self.obj.created_at, datetime)


class CartItemModelTest(TestCase):
    def setUp(self):
        self.cart = Cart()
        self.cart.save()
        self.article = Article(
            name='water',
            price='100'
        )
        self.article.save()

        self.item = CartItem(cart=self.cart, article=self.article, quantity=2)
        self.item.save()

    def test_create_item(self):
        """CartItem objects must exist"""
        self.assertTrue(CartItem.objects.exists())

    def test_quantity(self):
        """CartItem quantity must be equal to 2"""
        self.assertEqual(self.item.quantity, 2)
