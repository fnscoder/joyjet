import datetime

from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from core.models import Article, Cart, DiscountType, Discount


class ArticleTest(APITestCase):
    def setUp(self):
        User.objects.create_user(
            username="joyjet",
            email="joyjet@email.com",
            password="joyjetpass",
        )
        url = reverse("api-jwt-auth")
        response = self.client.post(
            url, {
                "username": "joyjet",
                "password": "joyjetpass"
            },
            format="json"
        )
        token = response.data["token"]
        self.client.credentials(HTTP_AUTHORIZATION="JWT " + token)

        url = '/api/articles/'
        article1 = {
            'name': 'water',
            'price': 100
        }

        article2 = {
            'name': 'tea',
            'price': 200
        }

        self.response = self.client.post(url, article1, format='json')
        self.client.post(url, article2, format='json')

    def test_post_article(self):
        """Must return status code 201"""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_get_all_articles(self):
        """Must return status code 200 and retrieve 2 articles"""
        response = self.client.get('/api/articles/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Article.objects.count(), 2)
        self.assertContains(response, 'water')
        self.assertContains(response, 'tea')

    def test_get_one_article(self):
        """Must return status code 200 and contains water in response"""
        response = self.client.get('/api/articles/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'water')


class CartTest(APITestCase):
    def setUp(self):
        User.objects.create_user(
            username="joyjet",
            email="joyjet@email.com",
            password="joyjetpass",
        )
        url = reverse("api-jwt-auth")
        response = self.client.post(
            url, {
                "username": "joyjet",
                "password": "joyjetpass"
            },
            format="json"
        )
        token = response.data["token"]
        self.client.credentials(HTTP_AUTHORIZATION="JWT " + token)

        url = '/api/carts/'
        data = {"items": []}
        self.response = self.client.post(url, data)
        self.client.post(url, data)

    def test_post_cart(self):
        """Must return status code 201 created"""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_get_all_carts(self):
        """Must return status code 200 and get 2 objects"""
        response =  self.client.get('/api/carts/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Cart.objects.count(), 2)

    def test_get_one_cart(self):
        """Must return status code 200 and contains created_at attribute"""
        response = self.client.get('/api/carts/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'created_at')

    def test_not_found(self):
        """Must return status code 404"""
        response = self.client.get('/api/carts/99/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CartItemTest(APITestCase):
    def setUp(self):
        User.objects.create_user(
            username="joyjet",
            email="joyjet@email.com",
            password="joyjetpass",
        )
        url = reverse("api-jwt-auth")
        response = self.client.post(
            url, {
                "username": "joyjet",
                "password": "joyjetpass"
            },
            format="json"
        )
        token = response.data["token"]
        self.client.credentials(HTTP_AUTHORIZATION="JWT " + token)

        article_url = '/api/articles/'
        articles = [
                    {"name": "water", "price": 100},
                    {"name": "honey", "price": 200},
                    {"name": "mango", "price": 400},
                    {"name": "tea", "price": 1000},
                    {"name": "ketchup", "price": 999},
                    {"name": "mayonnaise", "price": 999},
                    {"name": "fries", "price": 378},
                    {"name": "ham", "price": 147}
            ]
        for article in articles:
            self.client.post(article_url, article, format='json')

        for cart in range(6):
            self.client.post('/api/carts/', {"items": []})

        items_cart1 = {
                        "items": [
                            {"article_id": 1, "quantity": 6},
                            {"article_id": 2, "quantity": 2},
                            {"article_id": 4, "quantity": 1}
                        ]
                    }
        items_cart2 = {"items": [{"article_id": 2, "quantity": 1}, {"article_id": 3, "quantity": 3}]}
        items_cart3 = {"items": [{"article_id": 5, "quantity": 1}, {"article_id": 6, "quantity": 1}]}
        items_cart4 = {"items": [{"article_id": 7, "quantity": 1}]}
        items_cart5 = {"items": [{"article_id": 8, "quantity": 3}]}
        self.cart1 = self.client.post('/api/carts/1/add_to_cart/', items_cart1, format='json')
        self.cart2 = self.client.post('/api/carts/2/add_to_cart/', items_cart2, format='json')
        self.cart3 = self.client.post('/api/carts/3/add_to_cart/', items_cart3, format='json')
        self.cart4 = self.client.post('/api/carts/4/add_to_cart/', items_cart4, format='json')
        self.cart5 = self.client.post('/api/carts/5/add_to_cart/', items_cart5, format='json')

    def test_post_add_item(self):
        """Must contains content of cart 1 and cart 2"""
        self.assertEqual(self.cart1.status_code, status.HTTP_200_OK)
        contents_cart1 = ('water', 6, 'honey', 2, 'tea', 1)

        for content in contents_cart1:
            self.assertContains(self.cart1, content)

        contents_cart2 = ('honey', 1, 'mango', 3)

        for content in contents_cart2:
            with self.subTest():
                self.assertContains(self.cart2, content)

    def test_get_cart_items(self):
        """Must return status code 200 and get all carts"""
        response = self.client.get('/api/cart_items/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        carts = ('"cart":{"id":1', '"cart":{"id":2')
        for cart in carts:
            with self.subTest():
                self.assertContains(response, cart)

    def test_checkout(self):
        """
        Must set checkout = true, caculate total value and set delivery date
        """
        response = self.client.get('/api/carts/1/checkout/')
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        cart = Cart.objects.get(pk=1)

        self.assertEqual(cart.checkout, True)
        self.assertEqual(cart.total, 2000)
        self.assertEqual(cart.delivery_date, tomorrow)
        self.assertContains(response, '{"id":1,"total":2000}')

    def test_discounts(self):
        """
        Must return number of discount types (2) and discounts (5)
        Must do checkout and check total value with discount
        """
        self.client.post('/api/discount_types/', {"discount_type": "amount"})
        self.client.post('/api/discount_types/', {"discount_type": "percentage"})
        self.client.post('/api/discounts/2/add_discount/', {"discount_type": 1, "value": 25})
        self.client.post('/api/discounts/5/add_discount/', {"discount_type": 2, "value": 30})
        self.client.post('/api/discounts/6/add_discount/', {"discount_type": 2, "value": 30})
        self.client.post('/api/discounts/7/add_discount/', {"discount_type": 2, "value": 25})
        self.client.post('/api/discounts/8/add_discount/', {"discount_type": 2, "value": 10})

        self.assertEqual(DiscountType.objects.count(), 2)
        self.assertEqual(Discount.objects.count(), 5)

        response_cart1 = self.client.get('/api/carts/1/checkout/')
        response_cart2 = self.client.get('/api/carts/2/checkout/')
        response_cart3 = self.client.get('/api/carts/3/checkout/')
        response_cart4 = self.client.get('/api/carts/4/checkout/')
        response_cart5 = self.client.get('/api/carts/5/checkout/')

        self.assertContains(response_cart1, '"total":2350')
        self.assertContains(response_cart2, '"total":1775')
        self.assertContains(response_cart3, '"total":1798')
        self.assertContains(response_cart4, '"total":1083')
        self.assertContains(response_cart5, '"total":1196')
