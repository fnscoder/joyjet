from django.db import models


class Article(models.Model):
    name = models.CharField('Name', max_length=100)
    price = models.PositiveIntegerField('Price')

    def __str__(self):
        return self.name


class Cart(models.Model):
    created_at = models.DateTimeField('Created at', auto_now_add=True)
    updated_at = models.DateTimeField('Updated at', auto_now=True)
    checkout = models.BooleanField('Checkout', default=False)
    delivery_date = models.DateField('Devilery date', null=True, blank=True)
    total = models.IntegerField('Total', default=0)

    def __str__(self):
        return str(self.pk)


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, related_name='items', on_delete=models.CASCADE, null=True, blank=True)
    article = models.ForeignKey(Article, related_name='items', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField('Quantity', default=1, null=True, blank=True)

    def __str__(self):
        return f'{self.article.name}, {self.quantity}'


class DiscountType(models.Model):
    discount_type = models.CharField('Discount type', max_length=20)

    def __str__(self):
        return self.discount_type


class Discount(models.Model):
    article = models.ForeignKey(Article, related_name='discounts', on_delete=models.CASCADE)
    discount_type = models.ForeignKey(DiscountType, related_name='discounts', on_delete=models.CASCADE)
    value = models.PositiveIntegerField('Value')

    def __str__(self):
        return f'{self.article}, {self.discount_type}, {self.value}'
