from django.urls import path, include

from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from .views import ArticleViewSet, CartViewSet, CartItemViewSet, DiscountViewSet, DiscountTypeViewSet


router = routers.DefaultRouter()
router.register('articles', ArticleViewSet)
router.register('carts', CartViewSet)
router.register('cart_items', CartItemViewSet)
router.register('discount_types', DiscountTypeViewSet)
router.register('discounts', DiscountViewSet)

urlpatterns = [
    path('auth/token/', obtain_jwt_token, name='api-jwt-auth'),
    path('', include(router.urls)),
]