from django.contrib import admin
from .models import Article, Cart, CartItem, Discount, DiscountType


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'price')


class CartAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'updated_at')


class CartItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'cart', 'article', 'quantity')


class DiscountTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'discount_type')


class DiscountAdmin(admin.ModelAdmin):
    list_display = ('id', 'article', 'discount_type', 'value')


admin.site.register(Article, ArticleAdmin)
admin.site.register(Cart, CartAdmin)
admin.site.register(CartItem, CartItemAdmin)
admin.site.register(DiscountType, DiscountTypeAdmin)
admin.site.register(Discount, DiscountAdmin)
