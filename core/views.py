import datetime

from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from .models import Article, Cart, CartItem, Discount, DiscountType
from .serializers import (
    ArticleSerializer,
    CartSerializer,
    CartItemSerializer,
    DiscountSerializer,
    DiscountTypeSerializer,
    CheckoutSerializer,
)


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class CartViewSet(viewsets.ModelViewSet):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    @detail_route(methods=['post', 'put'])
    def add_to_cart(self, request, pk=None):
        cart = self.get_object()
        for item in request.data['items']:
            try:
                article = Article.objects.get(pk=item['article_id'])
                quantity = int(item['quantity'])
            except Exception:
                return Response({'Status': 'Fail'})

            cart_item_exists = CartItem.objects.filter(cart=cart, article=article).first()
            if cart_item_exists:
                cart_item_exists.quantity += quantity
                cart_item_exists.save()
            else:
                cart_item = CartItem(cart=cart, article=article, quantity=quantity)
                cart_item.save()

        serializer = CartSerializer(cart)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def checkout(self, request, pk=None):
        cart = Cart.objects.get(pk=pk)
        if cart.checkout:
            serializer = CartSerializer(cart)
            return Response(serializer.data)

        delivery_date = datetime.date.today() + datetime.timedelta(days=1)
        cart.delivery_date = delivery_date

        items = CartItem.objects.filter(cart_id=cart.pk)
        for item in items:
            price = item.article.price
            discounted_price = self.get_discounted_price(item.article.pk, price)
            cart.total = cart.total + item.quantity * discounted_price

        delivery_fees = self.calculate_fees(cart.total)
        cart.total = cart.total + delivery_fees
        cart.checkout = True
        cart.save()
        return Response(CheckoutSerializer(cart).data)

    def get_discounted_price(self, pk, price):
        try:
            discount = Discount.objects.get(article=pk)
            if discount.discount_type.pk == 1:
                price = price - discount.value
            elif discount.discount_type.pk == 2:
                price = int(price - (price * discount.value)/100)
        except Exception:
            return price
        return price

    def calculate_fees(self, total):
        price = 0
        if total < 1000:
            price = 800
        elif total < 2000:
            price = 400
        return price


class CartItemViewSet(viewsets.ModelViewSet):
    queryset = CartItem.objects.all()
    serializer_class = CartItemSerializer


class DiscountTypeViewSet(viewsets.ModelViewSet):
    queryset = DiscountType.objects.all()
    serializer_class = DiscountTypeSerializer


class DiscountViewSet(viewsets.ModelViewSet):
    queryset = Discount.objects.all()
    serializer_class = DiscountSerializer

    @detail_route(methods=['post', 'put'])
    def add_discount(self, request, pk=None):
        discount = Discount(
            article=Article.objects.get(pk=pk),
            discount_type=DiscountType.objects.get(pk=request.data['discount_type']),
            value=request.data['value']
        )
        discount.save()

        serializer = DiscountSerializer(discount)
        return Response(serializer.data)
